from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest


class NewVisitorTest(unittest.TestCase):
	"""Tests the use of superlists for a new user"""
	def setUp(self):
		self.browser = webdriver.Firefox()

	def tearDown(self):
		"""the user enters the URL and the list is still there"""
		self.browser.quit()
	
	def test_can_start_a_list_and_retrieve_it_later(self):
		# The user has head about a new To-do list app, opens up a browser
		self.browser.get('http://localhost:8000')
		
		# The user notices the page title
		self.assertIn('To-Do', self.browser.title)
		header_text = self.browser.find_element_by_tag_name('h1').text
		self.assertIn('To-Do', header_text)

		# The user is invited to enter a to-do item straight away
		inputbox = self.browser.find_element_by_id('id_new_item')
		self.assertEqual(
			inputbox.get_attribute('placeholder'),
			'Enter a to-do item'
		)

		# the user enters "buy milk"
		inputbox.send_keys('buy milk')

		# the user hits enter, the page update, and now the page lists
		# 1: Buy milk
		inputbox.send_keys(Keys.ENTER)
		time.sleep(1)

		table = self.browser.find_element_by_id('id_list_table')
		rows = table.find_elements_by_tag_name('tr')
		self.assertTrue(
			any(row.text == '1: buy milk' for row in rows)
		)

		# There must be a textbox to enter more items
		self.fail('finish the test!')
		# The user enters "buy movie tickets"

		# the user hits enter, the page update, and now the page lists
		# 1: Buy milk
		# 2: buy movie tickets

		# The page generates a unique URL for this list

if __name__ == '__main__':
	unittest.main(warnings='ignore')
